# README #

### Requirement ###

* Python 2.7

### How do I get set up the service? ###

* Create the virtualenv and activate it
* Install the requirements using pip => `pip install requirements.txt`
* Run the app => `./daemon.py`

### How to use the app? ###

* Download and install `app-debug.apk`
* Open the app
* Fill `Phone Number` with international format
* Fill the `Text` which want to be reversed
* Press `Submit` button


## About Async ##

To make sure Twisted's reactor never block (asynchronous) both subprocess communication and HTTP communication, we use `communicate` in subprocess and `threads` in twisted.