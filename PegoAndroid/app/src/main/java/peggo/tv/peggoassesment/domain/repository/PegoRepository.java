package peggo.tv.peggoassesment.domain.repository;

import retrofit2.Call;

/**
 * Created by pratama
 * Date : Apr - 4/10/17
 * Project Name : PeggoAssesment
 */

public interface PegoRepository {
  Call<String> sendString(final String string);
}
