package peggo.tv.peggoassesment.presentation.home;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import peggo.tv.peggoassesment.R;
import peggo.tv.peggoassesment.data.PegoServices;
import peggo.tv.peggoassesment.presentation.home.presenter.HomePresenter;

public class HomeActivity extends AppCompatActivity implements HomeView {

  @BindView(R.id.etInputText) EditText inputText;
  @BindView(R.id.etInputPhone) EditText inputPhoneNumber;

  private HomePresenter presenter;
  private PegoServices services;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);

    services = PegoServices.Creator.newServices();
    presenter = new HomePresenter(this, services);
    presenter.attachView();
  }

  @Override protected void onStart() {
    super.onStart();
  }

  @Override protected void onStop() {
    super.onStop();
    presenter.detachView();
  }

  @Override protected void onDestroy() {
    super.onDestroy();
  }

  @OnClick(R.id.btnSubmit) void onBtnSubmitClick() {
    presenter.submitText(inputText.getText().toString(), inputPhoneNumber.getText().toString());
  }

  @Override public void showError(String text) {
    Toast.makeText(HomeActivity.this, text, Toast.LENGTH_SHORT).show();
  }

  @Override public void showData(String inputText) {
    Toast.makeText(HomeActivity.this, inputText, Toast.LENGTH_SHORT).show();
  }
}
