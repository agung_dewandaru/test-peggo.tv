package peggo.tv.peggoassesment.data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.reactivex.Flowable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import peggo.tv.peggoassesment.data.model.response.ResponseGet;
import peggo.tv.peggoassesment.data.model.sender.PogoSender;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by pratama
 * Date : Apr - 4/10/17
 * Project Name : PeggoAssesment
 */

public interface PegoServices {

  @POST("/sms/") Flowable<ResponseGet> sendString(@Body PogoSender sender);

  class Creator {
    public static PegoServices newServices() {
      Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

      HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
      httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

      OkHttpClient okHttpClient =
          new OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor).build();

      Retrofit retrofit = new Retrofit.Builder().baseUrl("http://45.63.77.4:8090/")
          .addConverterFactory(GsonConverterFactory.create(gson))
          .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
          .client(okHttpClient)
          .build();

      return retrofit.create(PegoServices.class);
    }
  }
}
