package peggo.tv.peggoassesment.presentation.home.presenter;

import android.text.TextUtils;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import peggo.tv.peggoassesment.data.PegoServices;
import peggo.tv.peggoassesment.data.model.sender.PogoSender;
import peggo.tv.peggoassesment.presentation.home.HomeView;

/**
 * Created by pratama
 * Date : Apr - 4/10/17
 * Project Name : PeggoAssesment
 */

public class HomePresenter {

  private static final String TAG = "HomePresenter";
  private HomeView view;
  private PegoServices services;
  private CompositeDisposable compositeDispose;

  public HomePresenter(HomeView view, PegoServices services) {
    this.view = view;
    this.services = services;
  }

  public void attachView() {
    compositeDispose = new CompositeDisposable();
  }

  public void detachView() {
    compositeDispose.dispose();
  }

  public void submitText(String inputText, String phoneNumber) {
    if (validation(inputText, phoneNumber)) {
      Disposable sendString = services.sendString(
          new PogoSender.Builder().phoneNumber(phoneNumber).txtNumber(inputText).build())
          .subscribeOn(Schedulers.io())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe(result -> view.showData(result.code),
              throwable -> view.showError(throwable.getLocalizedMessage()));

      compositeDispose.add(sendString);
    }
  }

  private boolean validation(String inputText, String phoneNumber) {
    if (phoneNumber == null || TextUtils.isEmpty(phoneNumber)) {
      view.showError("Phone number empty");
      return false;
    }

    if (inputText == null || TextUtils.isEmpty(inputText)) {
      view.showError("Text empty");
      return false;
    }

    return true;
  }
}
