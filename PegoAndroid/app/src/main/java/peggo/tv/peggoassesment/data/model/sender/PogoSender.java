package peggo.tv.peggoassesment.data.model.sender;

import com.google.gson.annotations.SerializedName;

/**
 * Created by pratama
 * Date : Apr - 4/11/17
 * Project Name : PegoAndroid
 */

public class PogoSender {
  @SerializedName("code") public String txtNumber;
  @SerializedName("phone") public String phoneNumber;

  private PogoSender(Builder builder) {
    txtNumber = builder.txtNumber;
    phoneNumber = builder.phoneNumber;
  }

  public static final class Builder {
    private String txtNumber;
    private String phoneNumber;

    public Builder() {
    }

    public Builder txtNumber(String val) {
      txtNumber = val;
      return this;
    }

    public Builder phoneNumber(String val) {
      phoneNumber = val;
      return this;
    }

    public PogoSender build() {
      return new PogoSender(this);
    }
  }
}
