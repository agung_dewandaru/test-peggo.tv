package peggo.tv.peggoassesment.presentation.home;

/**
 * Created by pratama
 * Date : Apr - 4/10/17
 * Project Name : PeggoAssesment
 */

public interface HomeView {
  void showError(String text);

  void showData(String inputText);
}
