# README #

### Requirement ###

* Python 2.7

### How do I get set up? ###

* Create the virtualenv and activate it
* Install the requirements using pip => `pip install requirements.txt`
* Run the app => `./daemon.py`
