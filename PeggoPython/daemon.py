#!/usr/bin/env python

import subprocess
import json
from txrestapi.resource import APIResource
from txrestapi.methods import GET, POST, PUT, ALL
from twisted.web.server import Site
from twisted.internet import reactor, threads
from twilio.rest import Client

auth_token = "4791458a96399562c63e34900fb0dc91"
account_sid = "AC76e03fcecb73dd9e4fc1b6441f8462a9"

client = Client(account_sid, auth_token)


class SMSResource(APIResource):

    def _send_text(self, text, phone):
        try:
            message = client.messages.create(
                to=phone,
                from_="+12067927208",
                body=text,
            )
            print message.sid
        except Exception as e:
            print e

    def reverse_text(self, text, phone):
        p = subprocess.Popen(
            ['rev'],
            stdout=subprocess.PIPE,
            stdin=subprocess.PIPE,
            stderr=subprocess.STDOUT
        )
        result = p.communicate(text)[0]
        return result

    @POST('^/sms/$')
    def set_info(self, request):
        data = json.loads(request.content.read())
        code = data['code']
        phone = data['phone']
        d = threads.deferToThread(
            self.reverse_text,
            text=code,
            phone=phone
        )
        d.addCallback(self._send_text, phone=phone)
        return json.dumps(data)


def main():
    print 'Serving on PORT: 8090'
    site = Site(SMSResource(), timeout=None)
    reactor.listenTCP(8090, site)
    reactor.run()

if __name__ == '__main__':
    main()
